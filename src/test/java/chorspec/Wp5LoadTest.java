/*
 * Copyright 2016 The CHOReVOLUTION project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package chorspec;

import java.io.File;
import java.util.ArrayList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLStreamException;

import org.junit.Test;

import eu.chorevolution.datamodel.Choreography;
import eu.chorevolution.datamodel.ChoreographyService;
import eu.chorevolution.datamodel.DeployableService;
import eu.chorevolution.datamodel.DeployedService;
import eu.chorevolution.datamodel.ServiceGroup;
import eu.chorevolution.datamodel.deployment.CloudNode;
import eu.chorevolution.datamodel.deployment.DeploymentInfo;

public class Wp5LoadTest {

        @Test
        public void test1(){
        try {

            File file = new File("/tmp/wp5.xml");
            JAXBContext jaxbContext = JAXBContext.newInstance(Choreography.class);

            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            Choreography c = (Choreography) jaxbUnmarshaller.unmarshal(file);
            
            ArrayList<DeploymentInfo> deploymentInfo = new ArrayList<DeploymentInfo>();
            CloudNode nodeinfo;

            nodeinfo=new CloudNode();
            nodeinfo.setCpus(2);
            nodeinfo.setHostname("chornode1");
            nodeinfo.setImage("af036b89-0ef4-48e9-99ac-51b0cf9c314e");
            nodeinfo.setIp("192.168.150.130");
            nodeinfo.setOs("Ubuntu");
            nodeinfo.setPrivateKey("/etc/enactment-engine/eekey.pem");
            nodeinfo.setRam(1024);
            nodeinfo.setState(1);
            nodeinfo.setStorage(10000);
            nodeinfo.setUser("ubuntu");
            nodeinfo.setZone("RegionOne");
            ArrayList<DeploymentInfo> dlist = new ArrayList<DeploymentInfo>();
            DeploymentInfo d=new DeploymentInfo();
            d.setNode(nodeinfo);
            d.setEndpoint("http://192.168.150.130:8080/");
            dlist.add(d);
            
            for (ServiceGroup sg: c.getServiceGroups()) {
                ArrayList<ChoreographyService> deployed_services = new ArrayList<ChoreographyService>();
                deployed_services.addAll(sg.getServices());
                
                for ( ChoreographyService s:sg.getServices()) {
                    if (s instanceof DeployableService) {
                        DeployedService ds=new DeployedService();
                        ds.setDependencies(((DeployableService) s).getDependencies());
                        ds.setName(s.getName());
                        ds.setRoles(s.getRoles());
                        ds.setPackageUrl(((DeployableService) s).getPackageUrl());
                        ds.setServiceType(((DeployableService) s).getServiceType());
                        ds.setPackageType(((DeployableService) s).getPackageType());
                        ds.setVersion(((DeployableService) s).getVersion());
                        ds.setLoggingServerAddress(((DeployableService) s).getLoggingServerAddress());
                        ds.setInstances(((DeployableService) s).getInstances());
                        ds.setDeploymentInfo(dlist);
                        deployed_services.add(ds);
                        deployed_services.remove(s);
                    }
                }
                sg.setServices(deployed_services);
            }
            System.out.println(c.getXML());

        } catch (JAXBException e) {
            e.printStackTrace();
        } catch (XMLStreamException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
}
