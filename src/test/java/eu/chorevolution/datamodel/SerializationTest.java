/*
 * Copyright 2016 The CHOReVOLUTION project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.datamodel;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import eu.chorevolution.datamodel.Choreography;
import eu.chorevolution.datamodel.ChoreographyService;
import eu.chorevolution.datamodel.DeployableService;
import eu.chorevolution.datamodel.ExistingService;
import eu.chorevolution.datamodel.PackageType;
import eu.chorevolution.datamodel.ServiceGroup;

public class SerializationTest {

    @Test
    public void test1() throws Exception {
        Choreography chor = new Choreography();
        chor.setId("test_choreography");
        ServiceGroup group = new ServiceGroup();
        List<ServiceGroup> groups = new ArrayList<>();
        groups.add(group);
        chor.setServiceGroups(groups);
        List<ChoreographyService> group_services = new ArrayList<>();
        group.setServices(group_services);
        DeployableService dep_srv = new DeployableService();
        dep_srv.setInstances(2);
        dep_srv.setName("deployable_service_test");
        dep_srv.setPackageType(PackageType.WAR);
        dep_srv.setPackageUrl("http://localhost:8080/test.war");
        group_services.add(dep_srv);

        ExistingService ex1 = new ExistingService();
        ex1.setName("existing_test_service");
        ex1.setUrl("http://www.example.org/test");
        group_services.add(ex1);

        String xml = chor.getXML();
        assertNotNull(xml);
    }
}
