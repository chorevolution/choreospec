/*
 * Copyright 2016 The CHOReVOLUTION project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.datamodel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import eu.chorevolution.datamodel.deployment.CloudNode;
import eu.chorevolution.datamodel.deployment.DeploymentInfo;
import java.util.List;

public class DeserializationTest extends AbstractTest {

    @Test
    public void wp5Before() throws Exception {
        Choreography choreography = Choreography.fromXML(getClass().getResourceAsStream("/before_wp5.choreospec"));
        assertNotNull(choreography);

        assertNull(choreography.getId());

        assertNotNull(choreography.getServiceGroups());
        assertFalse(choreography.getServiceGroups().isEmpty());
        assertEquals(1, choreography.getServiceGroups().size());

        ServiceGroup serviceGroup = choreography.getServiceGroups().get(0);
        assertNotNull(serviceGroup);
        assertNotNull(serviceGroup.getServices());
        assertFalse(serviceGroup.getServices().isEmpty());

        ChoreographyService service = findService(serviceGroup.getServices(), "TrafficInformation");
        assertNotNull(service);
        assertTrue(service instanceof ExistingService);
        ExistingService eService = (ExistingService) service;
        assertTrue(eService.getUrl().startsWith("http://"));

        service = findService(serviceGroup.getServices(), "cdclientSTApp");
        assertNotNull(service);
        assertTrue(service instanceof DeployableService);
        DeployableService dService = (DeployableService) service;
        assertEquals(ServiceType.COORDINATION_DELEGATE, dService.getServiceType());
        assertEquals(PackageType.ODE, dService.getPackageType());
        assertEquals(1, dService.getInstances());
        assertTrue(dService.getPackageUrl().startsWith("http://"));

        assertFalse(dService.getDependencies().isEmpty());
        assertEquals(1, dService.getDependencies().size());

        ServiceDependency dependency = dService.getDependencies().get(0);
        assertNotNull(dependency);
        assertEquals("cdtouristagent", dependency.getServiceSpecName());
    }

    @Test
    public void wp5After() throws Exception {
        Choreography choreography = Choreography.fromXML(getClass().getResourceAsStream("/after_wp5.choreospec"));
        assertNotNull(choreography);

        assertNotNull(choreography.getId());

        ServiceGroup serviceGroup = choreography.getServiceGroups().get(0);
        assertNotNull(serviceGroup);
        assertNotNull(serviceGroup.getServices());
        assertFalse(serviceGroup.getServices().isEmpty());

        ChoreographyService service = findService(serviceGroup.getServices(), "cdclientSTApp");
        assertNotNull(service);
        assertTrue(service instanceof DeployedService);
        DeployedService dService = (DeployedService) service;
        assertEquals(ServiceType.COORDINATION_DELEGATE, dService.getServiceType());
        assertEquals(PackageType.ODE, dService.getPackageType());
        assertEquals(1, dService.getInstances());
        assertTrue(dService.getPackageUrl().startsWith("http://"));

        assertNotNull(dService.getUrl());

        List<DeploymentInfo> deploymentInfos = dService.getDeploymentInfo();
        assertNotNull(deploymentInfos);
        assertEquals(1, deploymentInfos.size());

        DeploymentInfo deploymentInfo = deploymentInfos.get(0);
        assertNotNull(deploymentInfo.getEndpoint());

        CloudNode node = deploymentInfo.getNode();
        assertNotNull(node);
        assertNotNull(node.getCpus());
        assertNotNull(node.getIp());
        assertEquals("Ubuntu", node.getOs());
    }
}
