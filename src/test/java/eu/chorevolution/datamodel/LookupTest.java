/*
 * Copyright 2016 The CHOReVOLUTION project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.datamodel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import eu.chorevolution.chors.ChorSpecUtils;
import org.junit.Test;

public class LookupTest extends AbstractTest {

    @Test
    public void securityFilterLookup() throws Exception {
        Choreography chorSpec = Choreography.fromXML(getClass().getResourceAsStream("/after_wp5.choreospec"));
        assertNotNull(chorSpec);

        ChoreographyService poi =
                findService(chorSpec.getServiceGroups().get(0).getServices(), "Poi");
        assertNotNull(poi);
        assertEquals(
                "http://192.168.150.142/sfpoi",
                ChorSpecUtils.findSecurityFilterURL(chorSpec, poi));

        ChoreographyService trafficInformation =
                findService(chorSpec.getServiceGroups().get(0).getServices(), "TrafficInformation");
        assertNotNull(trafficInformation);
        assertEquals(
                "http://192.168.150.142/sftrafficinformation",
                ChorSpecUtils.findSecurityFilterURL(chorSpec, trafficInformation));
    }
}
