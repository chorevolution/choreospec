/*
 * Copyright 2016 The CHOReVOLUTION project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.chors;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;

@WebService(
        name = "ConfigurableService",
        targetNamespace = "http://services.chorevolution.eu/"
)
public class Base implements BaseService {

    protected Map<String, String> address_map = null;

    protected String ENDPOINTADDRESS = null;

    /**
     * Each BasicService subclass has the following lifecycle:
     * 1. start the service
     * 2. send it its own enpoint address
     * 3. send the service dependency data (according to Enactment Engine choreography requirements)
     * */
    public Base() {
        address_map = new HashMap<>();
    }

    /* (non-Javadoc)
     * @see org.choreos.services.Base#setInvocationAddress(java.lang.String, java.lang.String)
     */
 /* (non-Javadoc)
     * @see eu.chorevolution.enactment.dependencies.ConfigurableService#setInvocationAddress(java.lang.String,
     * java.lang.String, java.util.List)
     */
    @Override
    @WebMethod
    public void setInvocationAddress(String role, String name, List<String> endpoints) {
        if (address_map.containsKey(name)) {
            address_map.remove(name);
        }
        address_map.put(name, endpoints.get(0));
    }

    /**
     * Example call: (Client1) (getPort("Client1", "http://localhost/client1?wsdl", Client1.class));
     *
     * @param <T> generic parameter
     * @param service_name service name
     * @param wsdlLocation WSDL location
     * @param source_url source URL 
     * @param client_class client class
     * @return service port
     * @throws java.net.MalformedURLException if bad URL is passed
     */
    public static <T> T getPort(String service_name, String wsdlLocation, String source_url, Class<T> client_class)
            throws MalformedURLException, IOException {

        T retPort = null;
        QName serviceQName = null;
        URL serviceUrl = null;
        Service s;
        int err_count = 0;
        String wsdl_content = null;

        serviceQName = new QName(namespace, service_name + "Service");

        while (err_count < 5) {
            wsdl_content = new Scanner(new URL(wsdlLocation).openStream(), "UTF-8").useDelimiter("\\A").next();
            if ((wsdl_content == null) || (wsdl_content.contains("<html>")) || (wsdl_content.contains("<body>"))) {
                try {
                Thread.sleep(1000);
                } catch(InterruptedException e) {
                    // ignore
                }
                err_count++;
            } else {
                break;
            }
        }

        //System.out.println("[cfg] Service name: "+service_name+" - WSDL: "+wsdlLocation+" - Source URL: "+source_url);
        s = Service.create(serviceUrl, serviceQName);
        retPort = s.getPort(client_class);
        BindingProvider bp = (BindingProvider) retPort;
        bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, source_url);

        return retPort;
    }

}
