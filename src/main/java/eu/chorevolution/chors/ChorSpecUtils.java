/*
 * Copyright 2016 The CHOReVOLUTION project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.chors;

import eu.chorevolution.datamodel.Choreography;
import eu.chorevolution.datamodel.ChoreographyService;
import eu.chorevolution.datamodel.DeployedService;
import eu.chorevolution.datamodel.ServiceDependency;
import eu.chorevolution.datamodel.ServiceType;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public final class ChorSpecUtils {

    public static List<String> findGlobalSecurityFilterURLs(final Choreography chorSpec) {
        List<String> result = new ArrayList<>();
        chorSpec.getServiceGroups().forEach((serviceGroup) -> {
            result.addAll(serviceGroup.getServices().stream().
                    filter((ChoreographyService cs) -> cs instanceof DeployedService).
                    filter((ChoreographyService dep)
                            -> ((DeployedService) dep).getServiceType() == ServiceType.GLOBAL_SECURITY_FILTER).
                    map(dep -> ((DeployedService) dep).getUrl()).
                    collect(Collectors.toList()));
        });

        return result;
    }

    public static String findSecurityFilterURL(final Choreography chorSpec, final ChoreographyService service) {
        Set<ChoreographyService> candidates = new HashSet<>();
        chorSpec.getServiceGroups().forEach((serviceGroup) -> {
            candidates.addAll(serviceGroup.getServices().stream().
                    filter((ChoreographyService cs) -> cs instanceof DeployedService).
                    filter((ChoreographyService dep) -> ((DeployedService) dep).getDependencies() != null).
                    filter((ChoreographyService dep)
                            -> ((DeployedService) dep).getDependencies().stream().
                            anyMatch(
                                    (ServiceDependency dependency)
                                    -> dependency.getServiceSpecName().equals(service.getName()))).
                    collect(Collectors.toSet()));
        });

        String securityFilterURL = null;

        Set<ChoreographyService> bcCandidates = new HashSet<>();
        for (ChoreographyService candidate : candidates) {
            switch (((DeployedService) candidate).getServiceType()) {
                case SECURITY_FILTER:
                    securityFilterURL = ((DeployedService) candidate).getUrl();
                    break;

                case BINDING_COMPONENT:
                    chorSpec.getServiceGroups().forEach((serviceGroup) -> {
                        bcCandidates.addAll(serviceGroup.getServices().stream().
                                filter((ChoreographyService cs) -> cs instanceof DeployedService).
                                filter((ChoreographyService dep)
                                        -> ((DeployedService) dep).getServiceType() == ServiceType.SECURITY_FILTER).
                                filter((ChoreographyService dep)
                                        -> ((DeployedService) dep).getDependencies() != null).
                                filter((ChoreographyService dep)
                                        -> ((DeployedService) dep).getDependencies().stream().
                                        anyMatch(
                                                (ServiceDependency dependency)
                                                -> dependency.getServiceSpecName().equals(candidate.getName()))).
                                collect(Collectors.toSet()));
                    });
                    break;

                default:
            }
        }

        if (securityFilterURL == null && !bcCandidates.isEmpty()) {
            securityFilterURL = ((DeployedService) bcCandidates.iterator().next()).getUrl();
        }

        return securityFilterURL;
    }

    private ChorSpecUtils() {
        // private constructor for static utility class
    }
}
