/*
 * Copyright 2016 The CHOReVOLUTION project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.services.datamodel.qos;

import java.io.Serializable;

public class ResponseTimeMetric implements Serializable {

    private static final long serialVersionUID = -2529361057036197375L;

    private float acceptablePercentage;

    private float maxDesiredResponseTime;

    public float getAcceptablePercentage() {
        return acceptablePercentage;
    }

    public float getMaxDesiredResponseTime() {
        return maxDesiredResponseTime;
    }

    public void setAcceptablePercentage(final float acceptablePercentage) {
        this.acceptablePercentage = acceptablePercentage;
    }

    public void setMaxDesiredResponseTime(final float maxDesiredResponseTime) {
        this.maxDesiredResponseTime = maxDesiredResponseTime;
    }

}
