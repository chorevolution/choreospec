/*
 * Copyright 2016 The CHOReVOLUTION project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.services.datamodel.qos;

import java.io.Serializable;

public class DesiredQoS implements Serializable {

    private static final long serialVersionUID = 5137885233674831781L;

    private static final ScalePolicy DEFAULT_SCALE_POLICY = ScalePolicy.HORIZONTAL;

    public enum ScalePolicy {
        HORIZONTAL,
        VERTICAL

    }

    private ResponseTimeMetric responseTime;

    private ScalePolicy scalePolicy = null;

    public void setResponseTimeMetric(ResponseTimeMetric responseTime) {
        this.responseTime = responseTime;
    }

    public ResponseTimeMetric getResponseTimeMetric() {
        return responseTime;
    }

    public ScalePolicy getScalePolicy() {
        return (scalePolicy != null) ? scalePolicy : DEFAULT_SCALE_POLICY;
    }

    public void setScalePolicy(ScalePolicy scalePolicy) {
        this.scalePolicy = scalePolicy;
    }
}
