/*
 * Copyright 2016 The CHOReVOLUTION project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.chorevolution.datamodel.deployment;

public class DeploymentPolicy {
    private boolean ha=false;
    private int min_vm=1;
    private int max_vm=1;
    
    private PolicyType policy=PolicyType.ALL_IN_ONE_VM;

    public boolean isHa() {
        return ha;
    }

    public void setHa(boolean ha) {
        this.ha = ha;
    }

    public int getMin_vm() {
        return min_vm;
    }

    public void setMin_vm(int min_vm) {
        this.min_vm = min_vm;
    }

    public int getMax_vm() {
        return max_vm;
    }

    public void setMax_vm(int max_vm) {
        this.max_vm = max_vm;
    }

    public PolicyType getPolicy() {
        return policy;
    }

    public void setPolicy(PolicyType policy) {
        this.policy = policy;
    }
    
    
}
