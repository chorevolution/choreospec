/*
 * Copyright 2016 The CHOReVOLUTION project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.datamodel.deployment;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class DeploymentLocation {

    private String jclouds_address;

    private String tenant;

    private String username;

    private String password;

    private String cloud_endpoint;

    private NodeSpec vm_spec;

    public DeploymentLocation() {
    }

    public String getJclouds_address() {
        return jclouds_address;
    }

    public void setJclouds_address(final String jclouds_address) {
        this.jclouds_address = jclouds_address;
    }

    public String getTenant() {
        return tenant;
    }

    public void setTenant(final String tenant) {
        this.tenant = tenant;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(final String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public String getCloud_endpoint() {
        return cloud_endpoint;
    }

    public void setCloud_endpoint(final String cloud_endpoint) {
        this.cloud_endpoint = cloud_endpoint;
    }

    public NodeSpec getVm_spec() {
        return vm_spec;
    }

    public void setVm_spec(final NodeSpec vm_spec) {
        this.vm_spec = vm_spec;
    }

}
