/*
 * Copyright 2016 The CHOReVOLUTION project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.datamodel.deployment;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class NodeSpec {

    private String image;

    private String zone;

    private String user;

    private String privateKeyFilepath;

    public NodeSpec() {

    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPrivateKeyFilepath() {
        return privateKeyFilepath;
    }

    public void setPrivateKeyFilepath(String privateKeyFilepath) {
        this.privateKeyFilepath = privateKeyFilepath;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((image == null) ? 0 : image.hashCode());
        result = prime * result + ((zone == null) ? 0 : zone.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        NodeSpec other = (NodeSpec) obj;
        if (image == null) {
            if (other.image != null) {
                return false;
            }
        } else if (!image.equals(other.image)) {
            return false;
        }
        if (zone == null) {
            if (other.zone != null) {
                return false;
            }
        } else if (!zone.equals(other.zone)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "NodeSpec [image=" + image + ", zone=" + zone + "]";
    }

}
