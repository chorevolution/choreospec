/*
 * Copyright 2015 The CHOReVOLUTION project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.datamodel;

import java.io.InputStream;
import java.io.StringWriter;
import java.io.Writer;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;

import eu.chorevolution.datamodel.deployment.DeploymentLocation;
import eu.chorevolution.datamodel.deployment.DeploymentPolicy;

@XmlRootElement
public class Choreography {

    private static final JAXBContext CTX;

    private static final XMLInputFactory XML_INPUT_FACTORY = XMLInputFactory.newInstance();

    private static final XMLOutputFactory XML_OUTPUT_FACTORY = XMLOutputFactory.newInstance();

    static {
        try {
            CTX = JAXBContext.newInstance(Choreography.class);
        } catch (final JAXBException e) {
            throw new RuntimeException("Could not initialize JAXB", e);
        }
    }

    private String id;

    private String tenant;

    private List<ServiceGroup> serviceGroups;
    
    private List<ChoreographyService> services;

    private StatusType status;

    private DeploymentLocation location;
    private DeploymentPolicy  policy;
    
    private String logServerUrl;

    public Choreography() {
    }

    @XmlAttribute
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @XmlElement(name = "service_group")
    public List<ServiceGroup> getServiceGroups() {
        return serviceGroups;
    }

    public void setServiceGroups(final List<ServiceGroup> serviceGroups) {
        this.serviceGroups = serviceGroups;
    }

    public String getTenant() {
        return tenant;
    }

    public void setTenant(String tenant) {
        this.tenant = tenant;
    }

    public StatusType getStatus() {
        return status;
    }

    public void setStatus(StatusType status) {
        this.status = status;
    }

    public DeploymentLocation getLocation() {
        return location;
    }

    public void setLocation(DeploymentLocation location) {
        this.location = location;
    }


    
    public String getXML() throws JAXBException, XMLStreamException {
        Marshaller marshaller = CTX.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

        Writer writer = new StringWriter();
        marshaller.marshal(this, XML_OUTPUT_FACTORY.createXMLStreamWriter(writer));
        return writer.toString();
    }

    public static Choreography fromXML(final InputStream is) throws JAXBException, XMLStreamException {
        return CTX.createUnmarshaller().
                unmarshal(XML_INPUT_FACTORY.createXMLStreamReader(is), Choreography.class).getValue();
    }

    public String getLogServerUrl() {
        return logServerUrl;
    }

    public void setLogServerUrl(String logServerUrl) {
        this.logServerUrl = logServerUrl;
    }

    public List<ChoreographyService> getServices() {
        return services;
    }

    public void setServices(List<ChoreographyService> services) {
        this.services = services;
    }

    public DeploymentPolicy getPolicy() {
        return policy;
    }

    public void setPolicy(DeploymentPolicy policy) {
        this.policy = policy;
    }

}
