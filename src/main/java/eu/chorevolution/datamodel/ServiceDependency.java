/*
 * Copyright 2016 The CHOReVOLUTION project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.datamodel;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Represents the dependence of a service acting with a role.
 */
@XmlRootElement
public class ServiceDependency {

    private String serviceSpecName;

    private String serviceSpecRole;

    public ServiceDependency() {

    }

    public ServiceDependency(String serviceSpecName, String serviceRole) {
        this.serviceSpecName = serviceSpecName;
        this.serviceSpecRole = serviceRole;
    }

    public String getServiceSpecName() {
        return serviceSpecName;
    }

    public void setServiceSpecName(String choreographyServiceSpecName) {
        this.serviceSpecName = choreographyServiceSpecName;
    }

    public String getServiceSpecRole() {
        return serviceSpecRole;
    }

    public void setServiceSpecRole(String choreographyServiceSpecRole) {
        this.serviceSpecRole = choreographyServiceSpecRole;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((serviceSpecName == null) ? 0 : serviceSpecName.hashCode());
        result = prime * result + ((serviceSpecRole == null) ? 0 : serviceSpecRole.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ServiceDependency other = (ServiceDependency) obj;
        if (serviceSpecName == null) {
            if (other.serviceSpecName != null) {
                return false;
            }
        } else if (!serviceSpecName.equals(other.serviceSpecName)) {
            return false;
        }
        if (serviceSpecRole == null) {
            if (other.serviceSpecRole != null) {
                return false;
            }
        } else if (!serviceSpecRole.equals(other.serviceSpecRole)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ServiceDependency [name=" + serviceSpecName + ", role=" + serviceSpecRole + "]";
    }

}
